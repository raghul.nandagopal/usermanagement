/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gozen.reach.user.web.rest.vm;
